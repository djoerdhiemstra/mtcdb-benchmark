/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.exception;
/**
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public class NoSuchAttributeException extends MTCBException {

	private static final long serialVersionUID = 1L;

	public NoSuchAttributeException(String type, String attr) {
		super("No such attribute: " + attr + " on type: " + type);
	}
}
