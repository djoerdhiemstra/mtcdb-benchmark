/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.system;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Map;
import java.util.UUID;

import com.actfact.mtcb.exception.NoSuchAttributeException;
import com.actfact.mtcb.model.Attribute;
import com.actfact.mtcb.model.AttributeValue;
import com.actfact.mtcb.model.Ctx;
import com.actfact.mtcb.model.PO;
import com.actfact.mtcb.model.Tenant;
import com.actfact.mtcb.model.Trx;
import com.actfact.mtcb.model.Type;
import com.actfact.mtcb.model.User;

/**
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public enum SystemUser implements User {
	SYSTEM;

	private final UUID id;
	private final Timestamp created;
	private final String name;
	private final String email;
	private final Ctx ctx;

	private SystemUser()
	{
		id = new UUID(2,1);
		created = new Timestamp(0);
		name = "System";
		email = "w.vanderzijden@actfact.com";
		ctx = new Ctx(SystemTenant.SYSTEM, this);
	}
	
	@Override
	public UUID getID() {
		return id;
	}

	@Override
	public Tenant getTenant() {
		return SystemTenant.SYSTEM;
	}

	@Override
	public Timestamp getCreated() {
		return created;
	}

	@Override
	public User getCreatedBy() {
		return this;
	}

	@Override
	public Timestamp getUpdated() {
		return created;
	}

	@Override
	public User getUpdatedBy() {
		return this;
	}

	@Override
	public boolean isActive() {
		return true;
	}

	@Override
	public String getValueAsString(String attributeName) {
		throw new NoSuchAttributeException("User", attributeName);
	}

	@Override
	public <T extends PO> T getValueAsPO(String attributeName) {
		throw new NoSuchAttributeException("User", attributeName);
	}

	@Override
	public BigDecimal getValueAsNumber(String attributeName) {
		throw new NoSuchAttributeException("User", attributeName);
	}

	@Override
	public Boolean getValueAsBoolean(String attributeName) {
		throw new NoSuchAttributeException("User", attributeName);
	}

	@Override
	public Timestamp getValueAsTimestamp(String attributeName) {
		throw new NoSuchAttributeException("User", attributeName);
	}

	@Override
	public boolean isNew() {
		return false;
	}

	@Override
	public String getEmail() {
		return email;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Type getType() {
		return SystemType.USER;
	}

	@Override
	public Ctx getContext()
	{
		return ctx;
	}

	@Override
	public boolean save()
	{
		return true;
	}

	@Override
	public Trx getTrx()
	{
		return null;
	}

	@Override
	public void setNew(boolean isNew)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setActive(boolean isActive)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setString(String attributeName, String string)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public <T extends PO> void setPO(String attributeName, T t)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setBoolean(String attributeName, Boolean bool)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setTimestamp(String attributeName, Timestamp timestamp)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setEmail(String email)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setName(String name)
	{
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void setObject(String attributeName, Object object)
	{
		throw new UnsupportedOperationException();		
	}

	@Override
	public PO idToPO(Attribute attr, UUID id)
	{
		throw new UnsupportedOperationException();
	}
	
	@Override
	public Map<String, AttributeValue> getValues()
	{
		return null;
	}

}
