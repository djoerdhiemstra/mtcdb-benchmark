/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.model;

/**
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public interface Tenant extends PO
{
	/**
	 * A textual identifier for this tenant. Must be globally unique.
	 * 
	 */
	default String getName()
	{
		return getValueAsString("Name");
	}
	
	/**
	 * A name used for display to the user.
	 * 
	 */
	default String getDisplayName()
	{
		return getValueAsString("DisplayName");
	}
	
	/**
	 * Whether this Tenant is a module. A module only contains metadata, so no
	 * master data and no transaction data. 
	 * 
	 */
	default boolean isModule()
	{
		return getValueAsBoolean("IsModule");
	}
	
	/**
	 * Set the name.
	 * 
	 */
	default void setName(String name)
	{
		setString("Name", name);
	}
	
	/**
	 * Set the display name.
	 * 
	 */
	default void setDisplayName(String displayName)
	{
		setString("DisplayName", displayName);
	}
	
	/**
	 * Set whether this tenant is a module. 
	 */
	default void setModule(boolean isModule)
	{
		setBoolean("IsModule", isModule);
	}
	
}
