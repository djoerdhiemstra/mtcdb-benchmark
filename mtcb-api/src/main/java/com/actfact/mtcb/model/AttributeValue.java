/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.model;

/**
 * Convenience class to store Attribute/Value pairs.
 * <p>
 * Use the constructor to load existing data. When creating new instances,
 * use null as value.
 * <p>
 * Use setValue to change the value later. This will make isChanged() return true.
 * <p>
 * isNew() returns yes when the initial value is null
 * <p>
 * Call isSaved() after persisting this attribute/value pair to make isNew and isChanged return false.
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public class AttributeValue
{
	private final Attribute attr;
	private Object value;
	private Object newValue;
	private boolean isChanged;
	
	public AttributeValue(Attribute attr, Object value)
	{
		this.attr = attr;
		this.value = value;
	}
	
	public void setValue(Object value)
	{
		this.isChanged = true;
		newValue = value;
	}
	
	public Object getValue()
	{
		if (isChanged)
			return newValue;
		return value;
	}
	
	public Attribute getAttribute()
	{
		return attr;
	}
	
	public boolean isNew()
	{
		return value == null;
	}
	
	public void isSaved()
	{
		value = newValue;
		isChanged = false;
	}
	
	public boolean isChanged()
	{
		return isChanged;
	}
}
