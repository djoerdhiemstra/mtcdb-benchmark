/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.util;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public class Util
{
	private static Random rnd = new Random();
	
	/**
	 * Floyd's algorithm for selecting a random sample.
	 * 
	 * Courtesy of Eyal Schneider (https://eyalsch.wordpress.com/2010/04/01/random-sample/)
	 * 
	 */
	public static <T> Set<T> randomSample(List<T> items, int m){
	    HashSet<T> res = new HashSet<T>(m);
	    int n = items.size();
	    for(int i=n-m;i<n;i++){
	        int pos = rnd.nextInt(i+1);
	        T item = items.get(pos);
	        if (res.contains(item))
	            res.add(items.get(i));
	        else
	            res.add(item);
	    }
	    return res;
	}
}
