/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.schemabased;

import java.lang.reflect.Constructor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import com.actfact.mtcb.database.DB;
import com.actfact.mtcb.database.DB.Driver;
import com.actfact.mtcb.database.TrxImpl;
import com.actfact.mtcb.database.UncheckedSQLException;
import com.actfact.mtcb.exception.MTCBException;
import com.actfact.mtcb.main.Timing;
import com.actfact.mtcb.model.Attribute;
import com.actfact.mtcb.model.AttributeValue;
import com.actfact.mtcb.model.Ctx;
import com.actfact.mtcb.model.MTCDB;
import com.actfact.mtcb.model.PO;
import com.actfact.mtcb.model.Tenant;
import com.actfact.mtcb.model.Trx;
import com.actfact.mtcb.model.Type;
import com.actfact.mtcb.model.User;
import com.actfact.mtcb.system.SystemAttribute;
import com.actfact.mtcb.system.SystemTenant;
import com.actfact.mtcb.system.SystemType;
import com.actfact.mtcb.system.SystemUser;

/**
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public class MTCDB_SchemaBased implements MTCDB
{

	private final DB.Driver driver;
	private final String dbName;
	private final String dbHost; 
	private final String dbUser;
	private final String dbPass;

	//private static Random rnd = new Random();

	private static final List<String> SYSTEM_SCHEMAS = Arrays.asList(
			new String[]
			{ "pg_catalog", "public", "information_schema", "pg_toast", "pg_temp_1", "pg_toast_temp_1" });

	private final Connection conn;
	
	public MTCDB_SchemaBased(Driver driver, String dbHost, String dbName, String dbUser, String dbPass)
	{
		super();
		this.driver = driver;
		this.dbName = dbName;
		this.dbHost = dbHost;
		this.dbUser = dbUser;
		this.dbPass = dbPass;
		try
		{
			conn = DB.getConnection(driver, dbHost, dbName, dbUser, dbPass);
		}
		catch (SQLException e)
		{
			throw new UncheckedSQLException(e);
		}

	}

	@Override
	public void close() throws Exception
	{
		conn.close();
	}

	@Override
	public void initializeSystem()
	{
		TrxImpl trx = TrxImpl.get(conn, "CreateSchemaBased");
		try
		{
			dropUserSchemas(trx);
			// Create System Tenant schema
			TenantImpl.save(SystemTenant.SYSTEM, true, trx);
			// Create table structure for complex system types
			Type[] types = new Type[]
			{ SystemType.TYPE, SystemType.ATTRIBUTE, SystemType.TENANT, SystemType.USER };
			for (Type type : types)
				TypeImpl.doDDL(type.getContext(), type, true, trx);
			trx.commit();
		}
		catch (SQLException e)
		{
			trx.rollback();
			throw new UncheckedSQLException(e);
		}
	}

	private static void dropUserSchemas(TrxImpl trx) throws SQLException
	{
		String sql = "SELECT schema_name " + "FROM information_schema.schemata";
		PreparedStatement pstmt = DB.prepareStatement(trx, sql);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next())
		{
			if (!SYSTEM_SCHEMAS.contains(rs.getString("schema_name")))
			{
				sql = "DROP SCHEMA " + rs.getString("schema_name") + " CASCADE";
				//System.out.println(sql);
				DB.executeUpdate(trx, sql);
				trx.commit();
			}
		}
	}

	@Override
	public Tenant createTenant(Ctx ctx, String name, String displayName, boolean isModule, Tenant... dependencies)
	{
		long started = System.nanoTime();
		Trx trx = TrxImpl.get(conn, "Create Tenant");
		Tenant tenant = new TenantImpl(ctx, trx);
		tenant.setName(name);
		tenant.setDisplayName(displayName);
		tenant.setModule(isModule);
		if (!saveAndCommit(tenant))
			throw new MTCBException("Could not save tenant: " + tenant);
		Ctx newCtx = new Ctx(tenant, SystemUser.SYSTEM, dependencies);
		addSystemModule(newCtx);
		addModules(newCtx, trx);
		//addModule(newCtx, SystemTenant.SYSTEM);
		//for (Tenant dependency : dependencies)
		//	addModule(newCtx, dependency);
		Timing.register("CREATE Tenant", System.nanoTime() - started);
		return tenant;
	}

	@Override
	public User createUser(Ctx ctx, String name, String email)
	{
		Trx trx = TrxImpl.get(conn, "Create user");
		User user = new UserImpl(ctx, trx);
		user.setName(name);
		user.setEmail(email);
		saveAndCommit(user);
		return user;
	}

	@Override
	public Type getType(Ctx ctx, String name)
	{
		return getByAndOrOr(TypeImpl.class, ctx, SystemType.TYPE,
				new AttributeValue[]
				{ new AttributeValue(SystemAttribute.TYPE_NAME, name) },
				null, 1, true).get(0);
	}

	@Override
	public Tenant getTenant(String name)
	{
		Ctx ctx = getContext(SystemTenant.SYSTEM, SystemUser.SYSTEM);
		return getByAndOrOr(TenantImpl.class, ctx, SystemType.TENANT,
				new AttributeValue[]
				{ new AttributeValue(SystemAttribute.TENANT_NAME, name) }
				, null, 1, true).get(0);
	}

	@Override
	public Type createType(Ctx ctx, String name, String displayName)
	{
		Trx trx = TrxImpl.get(conn, "Create Type");
		Type type = new TypeImpl(ctx, trx);
		type.setName(name);
		type.setDisplayName(displayName);
		type.save();
		trx.commit();
		return type;
	}

	@Override
	public PO createPO(Ctx ctx, Type type, AttributeValue... values)
	{
		Trx trx = TrxImpl.get(conn, "Instantiate PO");
		PO po = new POImpl(ctx, type, trx);
		for (AttributeValue av : values)
			po.setObject(av.getAttribute().getName(), av.getValue());
		po.save();
		trx.commit();
		return po;
	}

	@Override
	public Attribute createAttribute(Ctx ctx, Type masterType, Type dataType, String name, String displayName,
			boolean isIndexed, boolean isMandatory)
	{
		Trx trx = TrxImpl.get(conn, "Create Attribtue");
		Attribute attr = new AttributeImpl(ctx, trx);
		attr.setMasterType(masterType);
		attr.setDataType(dataType);
		attr.setName(name);
		attr.setDisplayName(displayName);
		attr.setIndexed(isIndexed);
		attr.setMandatory(isMandatory);
		attr.save();
		trx.commit();
		masterType.addAttributes(attr);
		return attr;
	}

	@Override
	public Ctx getContext(Tenant tenant, User user, Tenant... dependencies)
	{
		return new Ctx(tenant, user, dependencies);
	}

	@Override
	public void addModule(Ctx ctx, Tenant module)
	{
		throw new MTCBException("Not implemented yet");
	}
	
	public void addModules(Ctx ctx, Trx trx)
	{
		for (Type type : getTypes(ctx))
			TypeImpl.doDDL(ctx, type, true, trx);
	}
	
	public void addSystemModule(Ctx ctx)
	{
		Trx trx = getTrx("AddSystemModule");
		TypeImpl.doDDL(ctx, SystemType.TYPE, true, trx);
		TypeImpl.doDDL(ctx, SystemType.ATTRIBUTE, true, trx);
		TypeImpl.doDDL(ctx, SystemType.USER, true, trx);
	}

	@Override
	public List<Tenant> getTenants()
	{
		Ctx ctx = getContext(SystemTenant.SYSTEM, SystemUser.SYSTEM);
		return getByAndOrOr(TenantImpl.class, ctx, SystemType.TENANT, null, null, 0,true);
	}

	@Override
	public List<Type> getTypes(Ctx ctx)
	{
		if (ctx.tenant == SystemTenant.SYSTEM)
			return Arrays.asList(SystemType.values());

		List<Type> result = getByAndOrOr(TypeImpl.class, ctx, SystemType.TYPE, null, null, 0, true);
		return result;
	}

	@Override
	public Trx getTrx(String name)
	{
		return TrxImpl.get(conn, name);
	}
	
	@Override
	public List<PO> getByConjunction(Ctx ctx, Type type, AttributeValue[] avs, Attribute[] orderAttributes, int limit)
	{
		return getByAndOrOr(null, ctx, type, avs, orderAttributes, limit, true);
	}
	
	@Override
	public List<PO> getByDisjunction(Ctx ctx, Type type, AttributeValue[] avs, Attribute[] orderAttributes, int limit)
	{
		return getByAndOrOr(null, ctx, type, avs, orderAttributes, limit, false);
	}
	
	public <T extends PO, K extends T> List<T> getByAndOrOr(Class<K> clazz, Ctx ctx, Type type,
			AttributeValue[] avs, Attribute[] orderAttributes, int limit, boolean isAnd)
	{
		Trx trx = getTrx("GetByConjunction");
		String sql = "SELECT * FROM " + POImpl.getTableName(ctx, type, true) + " t";
		if (avs != null && avs.length > 0)
		{
			sql += 	" WHERE ";
			for (int n = 0; n < avs.length; n++)
				sql += "t." + avs[n].getAttribute().getName() + " = ? "
				+ (isAnd ? "AND " : "OR  ");
			sql = sql.substring(0, sql.length() - 4);
		}
		if (orderAttributes != null && orderAttributes.length > 0)
		{
			String orderSql = "";
			for (Attribute orderAttr : orderAttributes)
				orderSql += ", t." + orderAttr.getName();
			sql += " ORDER BY " + orderSql.substring(1);
		}
		if (limit > 0)
			sql += " LIMIT " + limit;

		List<T> list = new ArrayList<>();
		try (PreparedStatement pstmt = DB.prepareStatement(trx, sql))
		{
			if (avs != null)
				for (int n = 0; n < avs.length; n++)
					if (avs[n].getValue() instanceof PO)
						pstmt.setObject(n + 1, ((PO) avs[n].getValue()).getID());
					else
						pstmt.setObject(n + 1, avs[n].getValue());

			//System.out.println(sql + (values != null ? Arrays.asList(values) : ""));

			Constructor<K> constructor = clazz == null ? null : clazz.getConstructor(Ctx.class, ResultSet.class, Trx.class);

			long started = System.nanoTime();
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				if (clazz == null)
					list.add((T) new POImpl(ctx, type, rs, trx));
				else
					list.add(constructor.newInstance(ctx, rs, trx));
			}
			Timing.register(
					"GET PO by " + (isAnd ? "Conjunction" : "Disjunction") + " of " 
							+ (avs == null ? 0 : avs.length) + " attribute" + (avs != null && avs.length == 1 ? "" : "s"),
							System.nanoTime() - started);
		}
		catch (SQLException e)
		{
			System.err.println(sql);
			throw new UncheckedSQLException(e);
		}
		catch (Exception e)
		{
			throw new MTCBException(e);
		}
		return list;
	}

	@Override
	public MTCDB getNewInstance()
	{
		return new MTCDB_SchemaBased(driver, dbHost, dbName, dbUser, dbPass);
	}

	@Override
	public long getSizeOnDisk()
	{
		Trx trx = getTrx("Get Size on Disk");
		String sql = "SELECT pg_database_size('" + dbName + "')";
		try (PreparedStatement pstmt = DB.prepareStatement(trx, sql))
		{
			ResultSet rs = pstmt.executeQuery();
			if (rs.next())
				return rs.getLong(1) / 1024 / 1024;			
			throw new MTCBException("No result for " + sql); 
		}
		catch (SQLException e)
		{
			throw new UncheckedSQLException(e);
		}
	}

	@Override
	public PO getPOByID(Ctx ctx, Type type, UUID id)
	{
		Trx trx = getTrx("Get PO By ID");
		return new POImpl(ctx, type, id, trx);
	}

}
