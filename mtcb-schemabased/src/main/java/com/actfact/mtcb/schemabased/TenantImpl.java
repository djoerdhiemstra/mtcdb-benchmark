/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.actfact.mtcb.schemabased;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import com.actfact.mtcb.database.DB;
import com.actfact.mtcb.model.Ctx;
import com.actfact.mtcb.model.Tenant;
import com.actfact.mtcb.model.Trx;
import com.actfact.mtcb.system.SystemType;

/**
 * 
 * @author Wim van der Zijden
 * @author ActFact Projects B.V.
 * (c) 2017
 */
public class TenantImpl extends POImpl implements Tenant
{
	public static final String TABLENAME = "Tenant";

	public TenantImpl(Ctx ctx, UUID id, Trx trx)
	{
		super(ctx, SystemType.TENANT, id, trx);
	}

	public TenantImpl(Ctx ctx, ResultSet rs, Trx trx) throws SQLException
	{
		super(ctx, SystemType.TENANT, rs, trx);
	}

	public TenantImpl(Ctx ctx, Trx trx)
	{
		super(ctx, SystemType.TENANT, trx);
	}

	@Override
	public boolean save()
	{
		// Value of isNew must be preserved
		boolean isNew = isNew();
		if (!super.save())
			return false;
		boolean result = save(this, isNew, getTrx());
		return result;
	}

	public static boolean save(Tenant tenant, boolean isNew, Trx trx)
	{
		if (isNew)
		{
			DB.executeUpdate(trx, "CREATE SCHEMA " + tenant.getName());
		}
		return true;
	}
	
}
